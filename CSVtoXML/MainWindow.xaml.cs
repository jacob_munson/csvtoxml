﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace CSVtoXML
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string folderPath = "";
            string fileName = "";

            openFileDialog.InitialDirectory = @"C:\";
            openFileDialog.Title = "Browse CSV Files";
            openFileDialog.DefaultExt = "csv";
            openFileDialog.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            if(openFileDialog.CheckFileExists == true)
            {
                //folderPath = openFileDialog.
            }
        }
    }
}
